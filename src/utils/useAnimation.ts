import { Ref, onMounted, onUnmounted } from 'vue';

export const useAnimation = (container: Ref) => {
  let observer: IntersectionObserver;
  onMounted(() => {
    observer = new IntersectionObserver(
      (entries) => {
        if (entries.length && entries[0].isIntersecting) {
          container.value.classList.add('active');
        } else {
          container.value.classList.remove('active');
        }
      },
      { threshold: 0.1 }
    );
    observer.unobserve;
    if (container.value) observer.observe(container.value);
  });
  onUnmounted(() => {
    if (observer !== undefined) observer.unobserve(container.value);
  });
};
