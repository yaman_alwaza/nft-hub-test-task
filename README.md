# Hash Include Task (hash-include-task)

Test task for frontend developer position in hash include company

## Install the dependencies

```bash
npm install
```

### Install Quasar CLI

```bash
npm install -g quasar
```

### Start the app in development ssr mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev -m ssr
```

### Build the app for production

```bash
quasar build
```
